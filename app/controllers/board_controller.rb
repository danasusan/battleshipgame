class BoardController < ApplicationController
  def index
    @board = Board.new
  end

  def generate
    redirect_to board_index_path
  end
end
