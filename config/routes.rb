Rails.application.routes.draw do
  get 'board/index'
  get 'board/generate'

  get 'player/index'

  root 'board#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
