class CreatePlayers < ActiveRecord::Migration[5.0]
  def change
    create_table :players do |t|
      t.string :name
      t.integer :score
      t.integer :games
      t.references :game, foreign_key: true

      t.timestamps null:false
    end
  end
end
